#!/bin/bash

mon1="DisplayPort-2"
mon2="HDMI-A-2"

# set up screens
xrandr --output $mon1 --left-of $mon2
xrandr --output $mon1 --primary

feh --bg-scale "/home/ben/Pictures/Wallpapers/2560x1080/$(ls ~/Pictures/Wallpapers/2560x1080 | shuf -n 1)" --bg-scale "/home/ben/Pictures/Wallpapers/1920x1080/$(ls ~/Pictures/Wallpapers/1920x1080 | shuf -n 1)"
#termite
numlockx on

# Run Polybar
killall polybar
polybar mon1 &
polybar mon2 &

# xidlehook
killall xidlehook
xidlehook --not-when-fullscreen --not-when-audio \
	  --timer normal 600 "slock" "" \
	  --timer normal 3600 "systemctl suspend" ""

kilalll mpd
mpd &

pulseaudio --start

killall compton
compton &
