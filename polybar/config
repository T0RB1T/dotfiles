=========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[colors]
;background = ${xrdb:color0:#222}
#background = #222
background	= #cc000000
background-alt	= #444
#foreground	= ${xrdb:color7:#222}
foreground	= #dfdfdf
foreground-alt	= #afafaf
primary		= #ff9900
secondary	= #e60053
alert		= #bd2c40
red   		= #ff6666
green 		= #66ff66
blue 		= #6666ff
orange 		= #e85005
yellow 		= #e3d212
purple 		= #d420d4
white		= #f0f0f0

[bar/mon1]
monitor = DisplayPort-2
width = 100%
height = 32
#offset-x = .5%
#offset-y = 1%
radius = 8.0
fixed-center = false

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 2
line-color = #f00

border-size = 2
border-color = #00000000

padding-left = 1
padding-right = 1

module-margin-left = 1
module-margin-right = 2

font-0 = Unifont
font-1 = Liberation Mono:size=20;0
font-2 = fixed:pixelsize=10;
font-3 = FontAwesome
font-4 = awesome-terminal-fonts
font-5 = Noto Sans Symbols2
font-6 = NotoSansMono Nerd Font

modules-left = bspwm
modules-center = mpd
modules-right = temperature filesystem pulseaudio cpu memory eth date powermenu

tray-position = right
# tray-padding = 0

wm-restack = bspwm

scroll-up = bspwm-desknext
scroll-down = bspwm-deskprev

[bar/mon2]
monitor = HDMI-A-2
width = 100%
height = 32
;offset-x = 1%
;offset-y = 1%
radius = 8.0
fixed-center = false

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = #f00

border-size = 2
border-color = #00000000

padding-left = 0
padding-right = 2

module-margin-left = 1
module-margin-right = 2

font-0 = Unifont
font-1 = Liberation Mono:size=20;0
font-2 = fixed:pixelsize=10;
font-3 = FontAwesome
font-4 = awesome-terminal-fonts
font-5 = Noto Sans Symbols2
font-6 = NotoSansMono Nerd Font

modules-left = bspwm
modules-right = pulseaudio date settings powermenu

wm-restack = bspwm

scroll-up = bspwm-desknext
scroll-down = bspwm-deskprev

[module/xwindow]
type = internal/xwindow
label = %title:0:30:...%

[module/filesystem]
#format-prefix = 
#format-prefix = 
type = internal/fs
interval = 25

mount-0 = /

label-mounted =  %{F#0a81f5}%mountpoint%%{F-}: %percentage_used%%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}

[module/bspwm]
type = internal/bspwm

label-focused = %name%
label-focused-foreground = ${colors.blue}
label-focused-background = ${colors.background-alt}
label-focused-underline= ${colors.orange}
label-focused-padding = 3

label-occupied = %name%
label-occupied-foreground = ${colors.blue}
label-occupied-underline= ${colors.yellow}
label-occupied-padding = 2

label-urgent = %name%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

label-empty = %name%
label-empty-foreground = ${colors.white}
label-empty-padding = 2

; Separator in between workspaces
label-separator = "|"

[module/mpd]
type = internal/mpd
format-online = <label-song>  <icon-prev> <icon-stop> <toggle> <icon-next>

icon-prev = ⏮
icon-stop = ⏹
icon-play = ⏯
icon-pause = ⏯
icon-next = ⏭

label-song-maxlen = 25
label-song-ellipsis = true

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = cpu: 
format-prefix-foreground = ${colors.foreground-alt}
format-underline = #f90000
label = %percentage:2%%

[module/memory]
type = internal/memory
interval = 2
format-prefix = "ram: "
format-prefix-foreground = ${colors.foreground-alt}
format-underline = #4bffdc
label = %percentage_used%%

[module/eth]
type = internal/network
interface = enp5s0
interval = 3.0

format-connected-underline = #55aa55
#format-connected-prefix = "eth "
format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.foreground-alt}
label-connected = %local_ip%

[module/date]
type = internal/date
interval = 5

date = " %Y-%m-%d"
date-alt  =

time = %H:%M:%S
time-alt = %H:%M

format-prefix = 🕑
format-prefix-foreground = ${colors.foreground-alt}
format-underline = #0a6cf5

label = %date% %time%

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <label-volume> <bar-volume>
label-volume = VOL %percentage%%
label-volume-foreground = ${root.foreground}

label-muted = VOL  🔇  | ─────────────────
label-muted-foreground = #666

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format = <ramp> <label>
format-underline = #f50a4d
format-warn = <ramp> <label-warn>
format-warn-underline = ${self.format-underline}

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = ${colors.secondary}

ramp-0 = "temp0"
ramp-1 = 
ramp-2 = "temp2"
ramp-foreground = ${colors.foreground-alt}

[module/powermenu]
type = custom/menu

expand-right = true

format-spacing = 1

label-open = "⏻"
label-open-foreground = ${colors.secondary}
label-close = close
label-close-foreground = ${colors.secondary}
label-separator = |
label-separator-foreground = ${colors.foreground-alt}

menu-0-0 = reboot
menu-0-0-exec = menu-open-1
menu-0-1 = shutdown
menu-0-1-exec = menu-open-2

menu-1-0 = back
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = shutdown -r now

menu-2-0 = power off
menu-2-0-exec = shutdown now
menu-2-1 = back
menu-2-1-exec = menu-open-0

[settings]
screenchange-reload = true
#compositing-background = xor
#compositing-background = screen
compositing-foreground = source
compositing-border = over
pseudo-transparency = false

[global/wm]
margin-top = 5
margin-bottom = 5

; vim:ft=dosini
