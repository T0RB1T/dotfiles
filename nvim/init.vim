if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

set number relativenumber
set nu rnu
set background = "dark"
set encoding=utf-8

call plug#begin()

Plug 'junegunn/fzf.vim'
Plug 'itchyny/lightline.vim'
Plug 'sjl/badwolf'
Plug 'justinmk/vim-dirvish'

call plug#end()

let g:airline_theme = "badwolf"
let colorscheme = "badwolf"

" Make the gutters darker than the background.
let g:badwolf_darkgutter = 1

" Make the tab line much lighter than the background.
let g:badwolf_tabline = 3
